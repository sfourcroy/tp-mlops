import os
import pytest
import pandas as pd
import joblib

dir_path = os.path.dirname(os.path.realpath(__file__))


def pytest_addoption(parser):
    parser.addoption(
        "--model-path", action="store", default=dir_path + "/../models/titanic.pkl"
    )
    parser.addoption(
        "--data-path", action="store", default=dir_path + "/../data/train.csv"
    )


@pytest.fixture
def model(pytestconfig):
    path = pytestconfig.getoption("model_path")
    model = joblib.load(path)  # pytest metafunc
    yield model
    assert os.path.exists(path)  # assert we did not remove it


@pytest.fixture
def dataset(pytestconfig):
    path = pytestconfig.getoption("data_path")
    df = pd.read_csv(path, index_col="PassengerId")
    yield df.drop("Survived", axis=1), df["Survived"]
    assert os.path.exists(path)
