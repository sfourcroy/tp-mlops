import pytest
import joblib
import pandas as pd
import os

from sklearn.metrics import accuracy_score


def test_accuracy(model, dataset):
    X, y_true = dataset
    y_pred = model.predict(X)
    assert accuracy_score(y_pred, y_true) > 0.6
